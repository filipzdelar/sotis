﻿using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sotis.Entities;
using Sotis.Models.Entities;

namespace Sotis.Data
{
    //public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>
    //{
    //public ApplicationDbContext(DbContextOptions options, IOptions<OperationalStoreOptions> operationalStoreOptions)
    //    : base(options, operationalStoreOptions)
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
              : base(options)
        {
        }

        //public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<Attempt> Attempts { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Users> Users { get; set; }
        public DbSet<Answare> Answares { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Test>().ToTable("Tests");
            modelBuilder.Entity<Attempt>().ToTable("Attempts");
            modelBuilder.Entity<Users>().ToTable("Users");
            modelBuilder.Entity<Question>().ToTable("Questions");
            modelBuilder.Entity<Answare>().ToTable("Answares");
        }

    }
}
