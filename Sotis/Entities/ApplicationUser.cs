﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sotis.Entities
{
    public class ApplicationUser : IdentityUser
    {
        [PersonalData]
        public string FirstName { get; set; }
        [PersonalData]
        public string LastName { get; set; }
        [PersonalData]
        public string Address { get; set; }
        [PersonalData]
        public string Country { get; set; }
        [PersonalData]
        public string Gender { get; set; }
        [PersonalData]
        public DateTime DateOfBirth { get; set; }
        [PersonalData]
        public string Website { get; set; }
        [PersonalData]
        public string Biography { get; set; }
        [PersonalData]
        public bool IsPrivate { get; set; }
        [PersonalData]
        public bool ReceiveMsgFromUnfollowed { get; set; }
        [PersonalData]
        public bool IsTagable { get; set; }
        [PersonalData]
        public bool IsVerified { get; set; }
        //notification settings
        [PersonalData]
        public bool FollowNotifications { get; set; }
        [PersonalData]
        public bool MsgNotifications { get; set; }
        [PersonalData]
        public bool PostNotifications { get; set; }
        [PersonalData]
        public bool CommentNotifications { get; set; }
        public List<ApplicationUser> BlockedUsers { get; set; }
    }
}
