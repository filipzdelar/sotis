﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Sotis.Models.Entities
{
    [Table("tbAttempts")]
    public class Attempt
    {
        [Key]
        public Guid AttemptId { get; set; }

        [ForeignKey("Test")]
        public Guid TestId { get; set; }

        public TimeSpan TakenTime { get; set; }
        public float Accuracy { get; set; }
        public int Grade { get; set; }
        
        public DateTime StartTime { get; set;}
        public DateTime EndTime { get; set; }
    }
}
