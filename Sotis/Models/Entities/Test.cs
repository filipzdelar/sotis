﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Sotis.Models.Entities
{
    [Table("tbTests")]
    public class Test
    {
        [Key]
        public Guid TestId { get; set; }
        
        public List<Question> Questions { get; set;}
        public TimeSpan TestDuration { get; set; }
    }
}
