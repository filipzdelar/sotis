﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Sotis.Models.Entities
{
    [Table("tbUsers")]
    public class Users
    {
        [Key]
        public Guid UserId { get; set; }
        
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Index { get; set; }
        
        //public List<Attempt> Attempts { get; set; }

        //public List<Test> TestsCreated { get; set; }
    }
}
