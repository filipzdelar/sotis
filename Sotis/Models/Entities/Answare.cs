﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Sotis.Models.Entities
{
    [Table("tbAnswares")]
    public class Answare
    {    
        [Key]
        public Guid AnswareId { get; set; }

        [ForeignKey("Question")]
        public Guid QuestionId { get; set; }

        public string Context { get; set; }
        public bool IsCorrect { get; set; }
    }
}
