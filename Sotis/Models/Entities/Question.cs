﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Sotis.Models.Entities
{
    [Table("tbQuestions")]
    public class Question
    {
        [Key]
        public Guid QuestionId { get; set; }

        [ForeignKey("Test")]
        public Guid TestId { get; set; }

        public string QuestionText { get; set; }
        public string Subject { get; set; }
    }
}
